from accounts.models import Characters, Attending, Raids, thread, posts
from django.contrib import admin

admin.site.register(Characters)
admin.site.register(Attending)
admin.site.register(Raids)
admin.site.register(thread)
admin.site.register(posts)