from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from userena.models import UserenaBaseProfile

class MyProfile(UserenaBaseProfile):
    user = models.OneToOneField(User,
                                unique=True,
                                verbose_name=_('user'),
                                related_name='my_profile')
    favourite_snack = models.CharField(_('favourite snack'),
                                       max_length=5)

class Characters(models.Model):
    owner = models.OneToOneField(User, verbose_name='user', related_name='player')
    class_type = models.CharField(blank=False, max_length=16)
    dkp = models.IntegerField()
    name = models.CharField(blank=False, max_length=32)
    spec = models.CharField(blank=False, max_length=16)

    def __unicode__(self):
        return self.name


class Attending(models.Model):
    character_id = models.ForeignKey('Characters')
    status = models.IntegerField(max_length=1, default=0, help_text='0 for not attending, 1 for might attending, 2 will attend') #0 = not attending, 1 = might attend, 2 = will attend
    raid_id = models.ForeignKey('Raids', blank=False)


class Raids(models.Model):
    name = models.CharField(max_length=34, blank=False)
    start_date = models.DateTimeField(auto_now_add=False)
    end_date = models.DateTimeField(auto_now_add=False)

    def __unicode__(self):
        return self.name

class thread(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    created_by = models.OneToOneField(User, verbose_name='user', related_name='op')
    category = models.CharField(max_length=32, blank=False)
    title = models.CharField(max_length=128, blank=False)
    message = models.TextField(blank=False)

    def __unicode__(self):
        return self.message

class posts(models.Model):
    message = models.TextField(blank=False)
    thread_id = models.ForeignKey('Thread', blank=False)
    date = models.DateTimeField(auto_now_add=True)
    poster = models.OneToOneField(User, verbose_name='user', related_name='poster')

    def __unicode__(self):
        return self.message
