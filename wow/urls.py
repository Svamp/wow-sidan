from django.conf.urls import patterns, include, url
from django import forms
from accounts.forms import SignupFormExtra

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.views.generic import TemplateView
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'WoW.views.home', name='home'),
    # url(r'^WoW/', include('WoW.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', TemplateView.as_view(template_name='index.html')),

    url(r'^members/$', TemplateView.as_view(template_name='Memberlist.html')),


    (r'^accounts/signup/$',
    'userena.views.signup',
    {'signup_form': SignupFormExtra}),

	(r'^accounts/', include('userena.urls')),
	
    url(r'^accounts/signup/$', 
		'userena.views.signup',
	),

	(r'^accounts/signin/$',
		'userena.views.signin',
	),

	(r'^accounts/signout/$',
		'userena.views.signout',
	),
)
